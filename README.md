# CMConfiguration
Each file contains
1. Cold cable calibration
2. Cavity kt

## File Names

Here we store the cold cable attenuation and the cavity calibration coefficients for all cryomodules tested at TS2.
Every cryomodule has two files:

1. `CMxx_configuration.csv` containing the measured cold cable attenuations and the cavity calibration coefficients reported by the vertical tests from the IK
2. `CMxx_reconfiguration.csv` containing the measured cold cable attenuation adn the cavity calibration coefficients evaluated at TS2 


