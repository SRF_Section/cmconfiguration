#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#
# File: CMConfigure.py
# Project: CryoModule measurement cold parameter configuration
# Created Date: Wednesday, 16th March 2022 
# Author: MuYuan Wang
# -----
# Last Modified:
# Modified By: MuYuan Wang 
# -----
# Copyright (c) 2020 European Spallation Source Eric
# 
# Use it at your risk!
# It's free software anyway
# -----
# HISTORY:
# Date      	By	Comments
# ----------	---	----------------------------------------------------------
#
# Main GUI Application
import sys
import csv
from matplotlib.backends.qt_compat import QtCore, QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import FigureCanvas, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import warnings
warnings.filterwarnings('ignore')
debug=True

"""=============================================="""
# main class for the inner form of the application
class MainWidget(QtWidgets.QWidget):
    def __init__(self,parent):
        super().__init__()
        self.parent=parent
        self.__controls()
        self.__layout()
        self.__bindings()

    def __controls(self):
        # Quit Button
        self.btnQuit=QtWidgets.QPushButton('Quit',self)
        self.btnQuit.setToolTip('Quit from Explorer')
        self.btnQuit.clicked.connect(self.parent.close)
        # Generate csv file Button
        self.btnGenFile=QtWidgets.QPushButton('Generate .csv file',self)
        self.btnGenFile.setToolTip('Generate csv format file')
        self.btnGenFile.clicked.connect(self.GenFile)
        # Label for Authors
        self.lblAuthors=QtWidgets.QLabel('SRF Section\nLinac Group\nAcceleration Division')
        self.lblAuthors.setAlignment(QtCore.Qt.AlignCenter)
        self.lblAuthors.setFont(QtGui.QFont('',12))
        # Title
        self.lbltitle=QtWidgets.QLabel('CM Cold Configuration')
        self.lbltitle.setAlignment(QtCore.Qt.AlignCenter)
        self.lbltitle.setFont(QtGui.QFont('Times New Roman',20,QtGui.QFont.Bold))
        # Text explanation
        self.lblcboCMs=QtWidgets.QLabel('Select CM:', self)        
        # CryoModule ComboBox
        self.cboCMs=QtWidgets.QComboBox(self)
        self.cboCMs.setToolTip('Selected cryomodule')
        self.cboCMs.currentIndexChanged.connect(self.onCMChange)
        self.cboCMs.setFixedWidth(100)
        #
        # There are two groups for cold configuration:
        #   1. CM cold cable attenuation;
        #   2. Cavities kt;
        font='font-size: 14px; font-weight: normal;'
        # 1. Cold cable attenuation Group
        self.grpColdAtt=QtWidgets.QGroupBox('Cold Cable Attenuation [dB]',self)
        self.grpColdAtt.setFont(QtGui.QFont('Times New Roman',16,QtGui.QFont.Bold))
        # Label/text for cavity1 cold cable attenuation
        self.lblAttCAV1=QtWidgets.QLabel('Cavity1',self)
        self.lblAttCAV1.setAlignment(QtCore.Qt.AlignCenter)
        self.lblAttCAV1.setStyleSheet(font)
        self.txtAttCAV1=QtWidgets.QLineEdit('',self)
        self.txtAttCAV1.setAlignment(QtCore.Qt.AlignCenter)
        self.txtAttCAV1.setFixedSize(100,20)
        self.txtAttCAV1.setStyleSheet(font)
        # Label/text for cavity2 cold cable attenuation
        self.lblAttCAV2=QtWidgets.QLabel('Cavity2',self)
        self.lblAttCAV2.setAlignment(QtCore.Qt.AlignCenter)
        self.lblAttCAV2.setStyleSheet(font)
        self.txtAttCAV2=QtWidgets.QLineEdit('',self)
        self.txtAttCAV2.setAlignment(QtCore.Qt.AlignCenter)
        self.txtAttCAV2.setFixedSize(100,20)
        self.txtAttCAV2.setStyleSheet(font)
        # Label/text for cavity3 cold cable attenuation
        self.lblAttCAV3=QtWidgets.QLabel('Cavity3',self)
        self.lblAttCAV3.setAlignment(QtCore.Qt.AlignCenter)
        self.lblAttCAV3.setStyleSheet(font)
        self.txtAttCAV3=QtWidgets.QLineEdit('',self)
        self.txtAttCAV3.setAlignment(QtCore.Qt.AlignCenter)
        self.txtAttCAV3.setFixedSize(100,20)
        self.txtAttCAV3.setStyleSheet(font)
        # Label/text for cavity4 cold cable attenuation
        self.lblAttCAV4=QtWidgets.QLabel('Cavity4',self)
        self.lblAttCAV4.setAlignment(QtCore.Qt.AlignCenter)
        self.lblAttCAV4.setStyleSheet(font)
        self.txtAttCAV4=QtWidgets.QLineEdit('',self)
        self.txtAttCAV4.setAlignment(QtCore.Qt.AlignCenter)
        self.txtAttCAV4.setFixedSize(100,20)
        self.txtAttCAV4.setStyleSheet(font)
        # 2. Cavities kt Group
        self.grpCavKt=QtWidgets.QGroupBox('Cavities kt',self)
        self.grpCavKt.setFont(QtGui.QFont('Times New Roman',16,QtGui.QFont.Bold))
        # Label/text for cavity1 kt
        self.lblKtCAV1=QtWidgets.QLabel('Cavity1',self)
        self.lblKtCAV1.setAlignment(QtCore.Qt.AlignCenter)
        self.lblKtCAV1.setStyleSheet(font)
        self.txtKtCAV1=QtWidgets.QLineEdit('',self)
        self.txtKtCAV1.setAlignment(QtCore.Qt.AlignCenter)
        self.txtKtCAV1.setFixedSize(100,20)
        self.txtKtCAV1.setStyleSheet(font)
        # Label/text for cavity2 kt
        self.lblKtCAV2=QtWidgets.QLabel('Cavity2',self)
        self.lblKtCAV2.setAlignment(QtCore.Qt.AlignCenter)
        self.lblKtCAV2.setStyleSheet(font)
        self.txtKtCAV2=QtWidgets.QLineEdit('',self)
        self.txtKtCAV2.setAlignment(QtCore.Qt.AlignCenter)
        self.txtKtCAV2.setFixedSize(100,20)
        self.txtKtCAV2.setStyleSheet(font)
        # Label/text for cavity3 kt
        self.lblKtCAV3=QtWidgets.QLabel('Cavity3',self)
        self.lblKtCAV3.setAlignment(QtCore.Qt.AlignCenter)
        self.lblKtCAV3.setStyleSheet(font)
        self.txtKtCAV3=QtWidgets.QLineEdit('',self)
        self.txtKtCAV3.setAlignment(QtCore.Qt.AlignCenter)
        self.txtKtCAV3.setFixedSize(100,20)
        self.txtKtCAV3.setStyleSheet(font)
        # Label/text for cavity4 kt
        self.lblKtCAV4=QtWidgets.QLabel('Cavity4',self)
        self.lblKtCAV4.setAlignment(QtCore.Qt.AlignCenter)
        self.lblKtCAV4.setStyleSheet(font)
        self.txtKtCAV4=QtWidgets.QLineEdit('',self)
        self.txtKtCAV4.setAlignment(QtCore.Qt.AlignCenter)
        self.txtKtCAV4.setFixedSize(100,20)
        self.txtKtCAV4.setStyleSheet(font)

    def __layout(self):
        # Cold cable attenuation group layout
        self.gridColdAtt=QtWidgets.QGridLayout()
        self.gridColdAtt.setSpacing(10)
        self.gridColdAtt.addWidget(self.lblAttCAV1, 0,0,1,1)
        self.gridColdAtt.addWidget(self.txtAttCAV1, 1,0,1,1)
        self.gridColdAtt.addWidget(self.lblAttCAV2, 0,1,1,1)
        self.gridColdAtt.addWidget(self.txtAttCAV2, 1,1,1,1)
        self.gridColdAtt.addWidget(self.lblAttCAV3, 0,2,1,1)
        self.gridColdAtt.addWidget(self.txtAttCAV3, 1,2,1,1)
        self.gridColdAtt.addWidget(self.lblAttCAV4, 0,3,1,1)
        self.gridColdAtt.addWidget(self.txtAttCAV4, 1,3,1,1)
        self.grpColdAtt.setLayout(self.gridColdAtt)
        # Cavities kt group layout
        self.gridCavKt=QtWidgets.QGridLayout()
        self.gridCavKt.setSpacing(10)
        self.gridCavKt.addWidget(self.lblKtCAV1,    0,0,1,1)
        self.gridCavKt.addWidget(self.txtKtCAV1,    1,0,1,1)
        self.gridCavKt.addWidget(self.lblKtCAV2,    0,1,1,1)
        self.gridCavKt.addWidget(self.txtKtCAV2,    1,1,1,1)
        self.gridCavKt.addWidget(self.lblKtCAV3,    0,2,1,1)
        self.gridCavKt.addWidget(self.txtKtCAV3,    1,2,1,1)
        self.gridCavKt.addWidget(self.lblKtCAV4,    0,3,1,1)
        self.gridCavKt.addWidget(self.txtKtCAV4,    1,3,1,1)
        self.grpCavKt.setLayout(self.gridCavKt)
        #Main Layout
        # --> Layout for the window
        #1 Layout of the top part
        self.hboxup=QtWidgets.QHBoxLayout()
        #2 Layout of the cold cable attenuation group
        self.hboxmi1=QtWidgets.QHBoxLayout()
        #3 Layout of the cavities kt group
        self.hboxmi2=QtWidgets.QHBoxLayout()
        #4 Layout of the bottom part
        self.hboxdo=QtWidgets.QHBoxLayout()
        #4 Vertical layout
        self.vbox=QtWidgets.QVBoxLayout()

        ### Fills in the layouts
        # 1 top part (centered label) H
        self.hboxup.addStretch(1)
        self.hboxup.addWidget(self.lblcboCMs)
        self.hboxup.addWidget(self.cboCMs)
        self.hboxup.addStretch(1)
        self.hboxup.addWidget(self.lbltitle)
        self.hboxup.addStretch(1)
        self.hboxup.addWidget(self.lblAuthors)
        self.hboxup.addStretch(1)
        # 2 middle part 1 H
        self.hboxmi1.addWidget(self.grpColdAtt)
        # 3 middle part 2 H
        self.hboxmi2.addWidget(self.grpCavKt)
        # 4 bottom part (quit button on right) H
        self.hboxdo.addStretch(1)
        self.hboxdo.addWidget(self.btnGenFile)
        self.hboxdo.addWidget(self.btnQuit)
        # 5 vertical layout (top, stretch, bottom) 
        self.vbox.addLayout(self.hboxup)
        self.vbox.addLayout(self.hboxmi1)
        self.vbox.addLayout(self.hboxmi2)
        self.vbox.addLayout(self.hboxdo)
        # 5 Apply Layout
        self.setLayout(self.vbox)

    def __bindings(self):
        self.cboCMs.currentIndexChanged.disconnect()
        self.cboCMs.clear()
        cmlist=['CM01','CM02','CM03','CM04','CM05','CM06','CM07','CM08','CM09',
                    'CM31','CM32','CM33','CM34','CM35','CM36','CM37','CM38','CM39']
        for d in cmlist:
            try:
                self.cboCMs.addItem(d)
            except:
                print('__bindings: Morto!')
        self.cboCMs.currentIndexChanged.connect(self.onCMChange)
        self.cboCMs.setCurrentIndex(0)
        self.onCMChange()

    # Cryomodule label change
    def onCMChange(self):
        self.selcm=self.cboCMs.currentText()

    # Generate csv file:
    #   1. get cold cable attenuation data
    #   2. get cavities kt data
    #   3. write to csv file
    def GenFile(self):
        try:
            attheader   = ['Cold cable attenuation:']
            attlist     = [[float(self.txtAttCAV1.text())],[float(self.txtAttCAV2.text())],[float(self.txtAttCAV3.text())],[float(self.txtAttCAV4.text())]]
            ktheader    = ['Cavities kt:']
            ktlist      = [[float(self.txtKtCAV1.text())],[float(self.txtKtCAV2.text())],[float(self.txtKtCAV3.text())],[float(self.txtKtCAV4.text())]]
        except ValueError:
            print('Invalid input data!')
            raise
        with open('{:s}_cold_configuration.csv'.format(self.selcm),'w',encoding='UTF8') as f:
            writer=csv.writer(f)
            # write the cold cable attenuation data
            writer.writerow(attheader)
            writer.writerows(attlist)
            # write the cavities kt data
            writer.writerow(ktheader)
            writer.writerows(ktlist)
            f.close()
        QtWidgets.QMessageBox.information(self,'OK!','{:s} Cold Config File is saved!'.format(self.selcm),
                    QtWidgets.QMessageBox.Ok ,QtWidgets.QMessageBox.Ok)

# The main window container class
"""================ (Main Window) ==============="""
class MainWindow(QtWidgets.QMainWindow,):
    def __init__(self, parent = None):
        super().__init__()
        #It contains just the MainWidget.
        self.main_widget = MainWidget(self)
        self.setCentralWidget(self.main_widget)
        self.initializeGUI()
    def initializeGUI(self):
        #Tooltips
        QtWidgets.QToolTip.setFont(QtGui.QFont('Times New Roman', 14))
        self.setToolTip('CM Cold Configuration')
        #Menu
        mnuExit=QtWidgets.QAction('&End Program',self)
        mnuExit.setStatusTip('Exit Explorer')
        mnuExit.triggered.connect(self.close)
        mnuSBar=QtWidgets.QAction('&View Statusbar',self,checkable=True)
        mnuSBar.setChecked(True)
        mnuSBar.triggered.connect(self.toggleSBView)
        menu=self.menuBar()
        mnufile=menu.addMenu('CM Configuration')
        mnufile.addAction(mnuExit)
        mnufile.addAction(mnuSBar)
        #StatusBar
        ##self.statusBar().showMessage('')
        # Resize main window
        self.resize(700,300)
        #self.center()
        self.setWindowTitle('CMConfiguration@ESS')
        self.show()
    """ Menu """
    def toggleSBView(self,state):
        if state:
            self.statusBar().show()
        else:
            self.statusBar().hide()
    """ Context menu """
    def contextMenuEvent(self, event):
        cmenu = QtWidgets.QMenu(self)
        quitAct = cmenu.addAction("Quit")
        # to map the coordinates so that 
        action = cmenu.exec_(self.mapToGlobal(event.pos()))
        if action == quitAct:
            self.close()
    def center(self):
        fg=self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        fg.moveCenter(cp)
        self.move(fg.topLeft())
    """ Traps the close Event """
    def closeEvent(self,event):
        reply = QtWidgets.QMessageBox.question(self,'Hej!','Are you sure to quit?',
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

""" MAIN APPLICATION ENTRY """
if __name__ == '__main__':
    qapp = QtWidgets.QApplication(sys.argv)
    w=MainWindow()
    w.show()
    qapp.exec_()